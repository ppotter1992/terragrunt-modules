###########Variables###########
variable "region" {
  description = "The region that the RDS will be deployed."
}

variable "region_primary" {
  description = "The primary region that the RDS will be deployed."
}

variable "region_secondary" {
  description = "The secondary region that the RDS will be deployed."
}

variable "global_cluster_identifier" {
  description = "The global cluster identifier specified on `aws_rds_global_cluster`"
  type        = string
  default     = null
}
variable "cluster_identifier_primary" {
  description = "The cluster identifier."
  type        = string
}

variable "cluster_identifier_secondary" {
  description = "The cluster identifier."
  type        = string
}

variable "kms_key_id" {
  description = "The ARN for the KMS encryption key. When specifying `kms_key_id`, `storage_encrypted` needs to be set to `true`"
  type = list(string)
}

variable "db_subnet_group_name" {
  description = "Name of DB subnet group. DB instance will be created in the VPC associated with the DB subnet group. If unspecified, will be created in the default VPC"
  type = list(string)
}

variable "vpc_security_group_ids" {
  description = "List of VPC security groups to associate"
  type = list(string)
}

variable "identifier_primary" {
  description = "The primary name of the RDS instance, if omitted, Terraform will assign a random, unique identifier"
}

variable "identifier_secondary" {
  description = "The secondary name of the RDS instance, if omitted, Terraform will assign a random, unique identifier"
}

variable "storage_encrypted" {
  description = "Specifies whether the DB instance is encrypted"
  type        = bool
}

variable "iam_database_authentication_enabled" {
  description = "Specifies whether or not the mappings of AWS Identity and Access Management (IAM) accounts to database accounts are enabled"
  type        = bool
}

variable "engine" {
  description = "The database engine to use"
  type        = string
}

variable "engine_mode" {
  description = "The database engine mode. Valid values: `global`, `multimaster`, `parallelquery`, `provisioned`, `serverless`. Defaults to: `provisioned`"
  type        = string
  default     = null
}

variable "engine_version" {
  description = "The engine version to use"
  type        = string
}


variable "copy_tags_to_snapshot" {
  description = "On delete, copy all Instance tags to the final snapshot (if final_snapshot_identifier is specified)"
  type        = bool
}

variable "instance_class" {
  description = "Instance type to use at master instance. Note: if `autoscaling_enabled` is `true`, this will be the same instance class used on instances created by autoscaling"
  type        = string
}

variable "database_name" {
  description = "Name for an automatically created database on cluster creation"
  type        = string
}

variable "master_username" {
  description = "Username for the master DB user"
  type        = string
  default     = "root"
}

variable "master_password" {
  description = "Password for the master DB user. Note - when specifying a value here, 'create_random_password' should be set to `false`"
  type        = string
  default     = null
}

variable "port" {
  description = "The port on which the DB accepts connections"
  type        = string
}

#variable "availability_zones" {
#  description = "List of availability zones."
#  type        = list(string)
#}

variable "tags" {
  description = "A mapping of tags to assign to all resources"
  type        = map(string)
}

variable "iops" {
  description = "The amount of provisioned IOPS. Setting this implies a storage_type of 'io1'"
  type        = number
  default     = null
}

variable "publicly_accessible" {
  description = "Bool to control if instance is publicly accessible"
  type        = bool
}

variable "allow_major_version_upgrade" {
  description = "Indicates that major version upgrades are allowed. Changing this parameter does not result in an outage and the change is asynchronously applied as soon as possible"
  type        = bool
}

variable "auto_minor_version_upgrade" {
  description = "Indicates that minor engine upgrades will be applied automatically to the DB instance during the maintenance window"
  type        = bool
}

variable "apply_immediately" {
  description = "Specifies whether any database modifications are applied immediately, or during the next maintenance window"
  type        = bool
}

variable "backup_retention_period" {
  description = "The days to retain backups for"
  type        = number
}

variable "preferred_backup_window" {
  description = "The daily time range during which automated backups are created if automated backups are enabled using the `backup_retention_period` parameter. Time in UTC"
  type        = string
}

variable "preferred_maintenance_window" {
  description = "The weekly time range during which system maintenance can occur, in (UTC)"
  type        = string
}

# DB parameter group

variable "db_cluster_parameter_group_name" {
  description = "A cluster parameter group to associate with the cluster"
  type        = string
  default     = null
}

variable "db_cluster_db_instance_parameter_group_name" {
  description = "Instance parameter group to associate with all instances of the DB cluster. The `db_cluster_db_instance_parameter_group_name` is only valid in combination with `allow_major_version_upgrade`"
  type        = string
  default     = null
}

variable "performance_insights_enabled" {
  description = "Specifies whether Performance Insights are enabled"
  type        = bool
}

#variable "performance_insights_kms_key_id" {
#  description = "The ARN for the KMS key to encrypt Performance Insights data"
#  type        = string
#}

variable "performance_insights_retention_period" {
  description = "The amount of time in days to retain Performance Insights data. Either 7 (7 days) or 731 (2 years)."
  type        = number
}

variable "ca_cert_identifier" {
  description = "Specifies the identifier of the CA certificate for the DB instance"
  type        = string
}

variable "deletion_protection" {
description = "The database can't be deleted when this value is set to true."
type = bool
}

variable "skip_final_snapshot" {
description = "Determines whether a final DB snapshot is created before the DB instance is deleted. If true is specified, no DBSnapshot is created. If false is specified, a DB snapshot is created before the DB instance is deleted, using the value from final_snapshot_identifier"
type = bool
}

variable "backtrack_window" {
  description = "The target backtrack window, in seconds. Only available for `aurora` engine currently. To disable backtracking, set this value to 0. Must be between 0 and 259200 (72 hours)"
  type        = number
}

variable "db_parameter_group_name" {
  description = "The name of the DB parameter group to associate with instances"
  type        = string
}

provider "aws" {
  region = var.region
}

provider "aws" {
  alias  = "primary"
  region = var.region_primary
}

provider "aws" {
  alias  = "secondary"
  region = var.region_secondary
}

locals {
  region_primary = var.region_primary
  region_secondary = var.region_secondary
}

provider "random" { }

resource "random_password" "password" {
  length           = 16
  special          = false
}

output "password" {
  description = "The password is:" 
  value = random_password.password.*.result
  sensitive = true
}

resource "aws_rds_global_cluster" "global" {
  global_cluster_identifier = var.global_cluster_identifier
  engine                    = var.engine
  engine_version            = var.engine_version
  database_name             = var.database_name
  storage_encrypted         = var.storage_encrypted
}

resource "aws_rds_cluster" "primary" {
  provider                                  = aws.primary
  global_cluster_identifier                 = aws_rds_global_cluster.global.global_cluster_identifier
  cluster_identifier                        = var.cluster_identifier_primary
  engine                                    = aws_rds_global_cluster.global.engine
  engine_version                            = aws_rds_global_cluster.global.engine_version
  engine_mode                               = var.engine_mode
  allow_major_version_upgrade               = var.allow_major_version_upgrade
  kms_key_id                                = var.kms_key_id[0]
  database_name                             = aws_rds_global_cluster.global.database_name
  master_username                           = var.master_username
  master_password                           = random_password.password.result
  skip_final_snapshot                       = var.skip_final_snapshot
  deletion_protection                       = var.deletion_protection
  backup_retention_period                   = var.backup_retention_period
  preferred_backup_window                   = var.preferred_backup_window
  availability_zones                        = ["${local.region_primary}a","${local.region_primary}b"]
  preferred_maintenance_window              = var.preferred_maintenance_window
  port                                      = var.port
  db_subnet_group_name                      = var.db_subnet_group_name[0]
  vpc_security_group_ids                    = tolist(["${var.vpc_security_group_ids[0]}"])
  apply_immediately                         = var.apply_immediately
  db_cluster_parameter_group_name           = var.db_cluster_parameter_group_name
  iam_database_authentication_enabled       = var.iam_database_authentication_enabled
  backtrack_window                          = var.backtrack_window
  copy_tags_to_snapshot                     = var.copy_tags_to_snapshot
}

resource "aws_rds_cluster_instance" "primary" {
  provider                              = aws.primary
  count                                 = 1
  identifier                            = "${var.identifier_primary}-${count.index}"
  cluster_identifier                    = aws_rds_cluster.primary.cluster_identifier
  engine                                = aws_rds_cluster.primary.engine
  engine_version                        = aws_rds_cluster.primary.engine_version
  instance_class                        = var.instance_class
  publicly_accessible                   = var.publicly_accessible
  db_subnet_group_name                  = aws_rds_cluster.primary.db_subnet_group_name
  db_parameter_group_name               = var.db_parameter_group_name
  apply_immediately                     = aws_rds_cluster.primary.apply_immediately
  preferred_maintenance_window          = aws_rds_cluster.primary.preferred_maintenance_window
  auto_minor_version_upgrade            = var.auto_minor_version_upgrade
  performance_insights_enabled          = var.performance_insights_enabled
# performance_insights_kms_key_id       = var.performance_insights_kms_key_id
  performance_insights_retention_period = var.performance_insights_retention_period
  copy_tags_to_snapshot                 = aws_rds_cluster.primary.copy_tags_to_snapshot
  ca_cert_identifier                    = var.ca_cert_identifier
}

 resource "aws_rds_cluster" "secondary" {
  depends_on = [aws_rds_cluster_instance.primary]
  provider                                  = aws.secondary
  global_cluster_identifier                 = aws_rds_global_cluster.global.global_cluster_identifier
  cluster_identifier                        = var.cluster_identifier_secondary
  engine                                    = aws_rds_global_cluster.global.engine
  engine_version                            = aws_rds_global_cluster.global.engine_version
  engine_mode                               = var.engine_mode
  allow_major_version_upgrade               = var.allow_major_version_upgrade
  kms_key_id                                = var.kms_key_id[1]
  source_region                             = var.region_primary
  skip_final_snapshot                       = var.skip_final_snapshot
  deletion_protection                       = var.deletion_protection
  backup_retention_period                   = var.backup_retention_period
  preferred_backup_window                   = var.preferred_backup_window
  availability_zones                        = ["${local.region_secondary}a","${local.region_secondary}b"]
  preferred_maintenance_window              = var.preferred_maintenance_window
  port                                      = var.port
  db_subnet_group_name                      = var.db_subnet_group_name[1]
  vpc_security_group_ids                    = tolist(["${var.vpc_security_group_ids[1]}"])
  apply_immediately                         = var.apply_immediately
  db_cluster_parameter_group_name           = var.db_cluster_parameter_group_name
  iam_database_authentication_enabled       = var.iam_database_authentication_enabled
  backtrack_window                          = var.backtrack_window
  copy_tags_to_snapshot                     = var.copy_tags_to_snapshot

  
}

resource "aws_rds_cluster_instance" "secondary" {
  provider                              = aws.secondary
  count                                 = 1
  identifier                            = "${var.identifier_secondary}-${count.index}"
  cluster_identifier                    = aws_rds_cluster.secondary.cluster_identifier
  engine                                = aws_rds_cluster.secondary.engine
  engine_version                        = aws_rds_cluster.secondary.engine_version
  instance_class                        = var.instance_class
  publicly_accessible                   = var.publicly_accessible
  db_subnet_group_name                  = aws_rds_cluster.secondary.db_subnet_group_name
  db_parameter_group_name               = var.db_parameter_group_name
  apply_immediately                     = aws_rds_cluster.secondary.apply_immediately
  preferred_maintenance_window          = aws_rds_cluster.secondary.preferred_maintenance_window
  auto_minor_version_upgrade            = var.auto_minor_version_upgrade
  performance_insights_enabled          = var.performance_insights_enabled
# performance_insights_kms_key_id       = var.performance_insights_kms_key_id
  performance_insights_retention_period = var.performance_insights_retention_period
  copy_tags_to_snapshot                 = aws_rds_cluster.secondary.copy_tags_to_snapshot
  ca_cert_identifier                    = var.ca_cert_identifier
}